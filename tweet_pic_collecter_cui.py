if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Get the favored image from Twitter.")
    parser.add_argument("screenname", help="Twitter User Name. @[screenname]")
    parser.add_argument(
        "-c",
        "--count",
        help="The number of cases to be traced back from the latest favorites. Default=20",
        type=int,
        default=20,
    )
    try:
        args = parser.parse_args()
    except SystemExit as e:
        if e == 2:
            pass
    else:
        from fav_img_dl import *
        for key in config["api_key"]["twitter"].values():
            if key=="":
                print("")
                print("Error: Enter your APIkey in the generated config file")
                sys.exit()
        favimagedownloader = FavImageDownloader()
        favimagedownloader.fav_img_dl(args.screenname, args.count)
