import os
import requests
import time


def download_image(url, timeout=10, retry_times=3):
    response = requests.get(url, allow_redirects=True, timeout=timeout)
    if response.status_code in [500, 502, 503,404]:
        time.sleep(2)
        for t in range(retry_times):
            response = requests.get(url, allow_redirects=True, timeout=timeout)
            if t < retry_times:
                if response.status_code in [500, 502, 503,404]:
                    time.sleep(2)
                    continue
            break
    if response.status_code != 200:
        e = Exception("HTTP status: " + str(response.status_code))
        raise e

    content_type = response.headers["content-type"]
    if "image" not in content_type:
        e = Exception("Content-Type: " + content_type)
        raise e

    return response.content


def save_image(image_path, image):
    image_dir = os.path.dirname(image_path)
    os.makedirs(image_dir, exist_ok=True)
    with open(image_path, "wb") as fout:
        fout.write(image)
