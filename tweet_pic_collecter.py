import sys

from pyinstaller_utils import *
from gui import *

def onCloseWindow(page, sockets):
    print(page, 'closed')
    print('Still have sockets open to', sockets)


cmdline_args =[]

eel.init(resource_path("resource/gui"))
eel.start("main.html", close_callback=onCloseWindow, cmdline_args=cmdline_args)

