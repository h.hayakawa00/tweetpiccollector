import re

from setting import *
from img_utils import *
from model import Author, Image, ProfileImage, Tag, Tweet, TweetTag, User


def get_user(screen_name):
    user = session.query(User).filter(User.screen_name == screen_name).first()
    return user


def set_user(name, screen_name, profile_image_url, profile_image_path):
    user = User()
    user.screen_name = screen_name
    user.name = name
    session.add(user)

    profile_image = ProfileImage()
    profile_image.path = profile_image_path
    profile_image.url = profile_image_url
    session.add(profile_image)
    session.flush()

    user.profile_image_id = profile_image.id

    session.commit()

    return user


def store_user(name, screen_name, profile_image_url, profile_image_path):
    user = get_user(screen_name)
    if user == None:
        user = set_user(name, screen_name, profile_image_url, profile_image_path)
    return user


def get_author(author_id):
    author = session.query(Author).filter(Author.id == author_id).first()
    return author


def set_author(author_id, screen_name, name, profile_image_url, profile_image_path):
    author = Author()
    author.id = author_id
    author.screen_name = screen_name
    author.name = name
    session.add(author)

    profile_image = ProfileImage()
    profile_image.path = profile_image_path
    profile_image.url = profile_image_url
    session.add(profile_image)
    session.flush()

    author.profile_image_id = profile_image.id

    session.commit()

    return author


def store_author(author_id, screen_name, name, profile_image_url, profile_image_path):
    author = get_author(author_id)
    if author == None:
        author = set_author(
            author_id, screen_name, name, profile_image_url, profile_image_path
        )
    return author


def get_tweet(tweet_id):
    tweet = session.query(Tweet).filter(Tweet.id == tweet_id).first()
    return tweet


def set_tweet(tweet_id, text, created_at, author_id, user_id):
    tweet = Tweet()
    tweet.id = tweet_id
    tweet.text = text
    tweet.created_at = created_at
    tweet.author_id = author_id
    tweet.user_id = user_id
    session.add(tweet)
    session.commit()
    return tweet


def store_tweet(tweet_id, text, created_at, author_id, user_id):
    tweet = get_tweet(tweet_id)
    if tweet == None:
        tweet = set_tweet(tweet_id, text, created_at, author_id, user_id)
    return tweet


def get_image(image_id):
    image = session.query(Image).filter(Image.id == image_id).first()
    return image


def set_image(image_id, image_url, image_path, tweet_id):
    image = Image()
    image.id = image_id
    image.path = image_path
    image.url = image_url
    image.tweet_id = tweet_id
    session.add(image)
    session.commit()

    return image


def store_image(image_id, image_url, image_path, tweet_id):
    image = get_image(image_id)
    if image == None:
        image = set_image(image_id, image_url, image_path, tweet_id)
    return image


def get_tag(hashtag):
    tag = session.query(Tag).filter(Tag.text == hashtag).first()
    return tag


def set_tag(tweet_id, hashtag):
    tag = Tag()
    tag.text = hashtag
    session.add(tag)
    session.flush()
    tweettag = TweetTag()
    tweettag.tweet_id = tweet_id
    tweettag.tag_id = tag.id
    session.add(tweettag)
    session.commit()
    return tag


def store_tag(tweet_id, hashtag):
    tag = get_tag(hashtag)
    if tag == None:
        tag = set_tag(tweet_id, hashtag)
    return tag


def get_tweettag(tweet_id, tag_id):
    tweettag = (
        session.query(TweetTag)
        .filter(TweetTag.tweet_id == tweet_id and TweetTag.tag_id == tag_id)
        .first()
    )
    return tweettag


def set_tweettag(tweet_id, tag_id):
    tweettag = TweetTag()
    tweettag.tweet_id = tweet_id
    tweettag.tag_id = tag_id
    session.add(tweettag)
    session.commit()
    return tweettag


def store_tweettag(tweet_id, tag_id):
    tweettag = get_tweettag(tweet_id, tag_id)
    if tweettag == None:
        tweettag = set_tweettag(tweet_id, tag_id)
    return tweettag
