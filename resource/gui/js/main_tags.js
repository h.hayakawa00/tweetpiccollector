var tag_text_old;

async function suggest() {
    console.log("run sugget")
    let tags_text_ele = document.getElementById("tag_text");
    let suggestbox_ele = document.getElementById("tag_suggest");
    let value = tags_text_ele.value;
    if (tag_text_old == value) {
        setTimeout(suggest, 500);
        return;
    }
    tag_text_old = value;
    if (value == "") {
        suggestbox_ele.style.display = "none";
        setTimeout(suggest, 500);
        return;
    } else {
        suggestbox_ele.style.display = "";
    }

    if (run_eel){
        setTimeout(suggest,500);
        return;
    }
    run_eel=true;
    let tags = await eel.get_tags(count = 10, index = value)();
    run_eel=false;

    suggestbox_ele.innerHTML = "";
    for (let tag of tags) {
        text = `<div id="tagsugget` + tag["id"] + `" onclick="onclick_suggest(this);">` + tag["text"] + `</div>`;
        suggestbox_ele.insertAdjacentHTML("beforeend", text);
    }
    setTimeout(suggest, 500);
}


function start_suggest() {
    tag_text_old = document.getElementById("tag_text").value;
    setTimeout(suggest, 500);
}



function onclick_suggest(obj) {
    let id = obj.id.match(/tagsugget(.+)/)[1];
    let taglist_ele = document.getElementById("taglist");
    let text = `
        <div class="tag" id="tag`+ id + `">
            <input type="button" class="sort-tag-remove-button" value="×" onclick="onclick_tag_remove(this);">
            <div class="tagtext">`+ obj.textContent + `</div>
        </div>`;
    taglist_ele.insertAdjacentHTML("beforeend", text);
    document.getElementById("tag_text").value = "";
    reset_tweet_col();
}

function onclick_tag_remove(obj) {
    obj.parentNode.remove();
    reset_tweet_col();
}