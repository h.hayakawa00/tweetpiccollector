var run_eel = false;
const sleep = msec => new Promise(resolve => setTimeout(resolve, msec));

async function main(){
    await set_userlist();
    await set_authorlist();
    await set_tweet_col();
    start_suggest();
}

main();