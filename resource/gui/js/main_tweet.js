var view_max_created_at = -1;
var run_set_tweet_col = false;


async function get_tweets(max_created_at = -1, tags = []) {
    let selected_user = document.getElementsByClassName("user selected")[0];
    let selected_user_id = "";
    if (selected_user.id != "user_all") {
        selected_user_id = selected_user.id.match(/user(.+)/)[1];
    }

    let selected_author = document.getElementsByClassName("author selected")[0];
    let selected_author_id = "";
    if (selected_author.id != "author_all") {
        selected_author_id = selected_author.id.match(/author(.+)/)[1];
    }

    tweets = await eel.get_tweets(50, max_created_at, selected_user_id, selected_author_id, tags)();
    return tweets
}

function get_taglist_id() {
    let tags_ele = document.getElementById("taglist").children;
    let tags = [];
    for (let tag_ele of tags_ele) {
        tags.push(tag_ele.id.match(/tag(.+)/)[1]);
    }
    return tags;
}

async function set_tweet_col() {
    while (run_eel) {
        await sleep(50);
    }
    run_eel = true;
    let taglist = get_taglist_id();

    let tweets = await get_tweets(view_max_created_at, taglist);

    let ele_tweet_col = document.getElementById("tweet_col");
    if (view_max_created_at == -1) {
        ele_tweet_col.innerHTML = ``;
    }
    for (let tweet of tweets) {
        let image_len = tweet["images"].length;
        let img_ele_text = ``;
        for (let i = 0; i < image_len; i++) {
            img_ele_text += `<img src="` + tweet["images"][i]["image"] + `" alt="" id="smallimg` + tweet["images"][i]["id"] + `">`
        }
        let created_at = new Date(tweet["created_at"] * 1000);
        let text = `
            <div class="tweet" id="tweet`+ tweet["id"] + `" onclick="onclick_tweet(this)">
                <div class="tweet-images img-`+ image_len + `">` + img_ele_text + `</div>
                <div class="tweet-desc">
                    <div class="tweet-text">`+ tweet["text"] + `</div>
                    <div class="tweet-author">`+ tweet["author"]["name"] + `</div>
                    <div class="tweet-option">`+ created_at.toString() + `</div>
                </div>
                <div class="tweet-text"></div>
            </div>`;
        ele_tweet_col.insertAdjacentHTML("beforeend", text);
    }
    console.log(tweets)

    if (tweets.length != 0) {
        view_max_created_at = tweets[tweets.length - 1]["created_at"];
    }

    run_eel = false;
}

async function reset_tweet_col() {
    view_max_created_at = -1;
    await set_tweet_col();
}

async function onclick_tweet(obj) {
    let id = obj.id.match(/tweet(.+)/)[1];

    while (run_eel) {
        await sleep(50);
    }
    run_eel = true;
    tweet = await eel.get_tweet(id)();
    run_eel = false;

    let ele_images = document.getElementById("images");
    ele_images.innerHTML="";
    for (let image of tweet["images"]) {
        let text = `<img src="` + image["image"] + `" alt="" id="img` + image["id"] + `">`;
        ele_images.insertAdjacentHTML("beforeend", text);
    }
    let text = `<img src="` + tweet["author"]["image"] + `" alt="">`;
    document.getElementById("images_author_image").innerHTML=text;
    document.getElementById("images_author_name").innerHTML= tweet["author"]["name"];
    document.getElementById("images_author_screenname").innerHTML= tweet["author"]["screen_name"];
    let created_at = new Date(tweet["created_at"] * 1000);
    document.getElementById("images_date").innerHTML= created_at.toString();
    document.getElementById("images_text").innerHTML= tweet["text"];

    document.getElementById("images_view").style.display = "block";
}



var run_scroll_update = false;
document.getElementById("tweet_col").addEventListener('scroll', async function () {
    if (run_scroll_update) {
        return;
    }
    run_scroll_update = true;

    let ele = document.getElementById("tweet_col");
    let height = ele.offsetHeight;
    let scroll_height = ele.scrollHeight;
    let scroll_top = ele.scrollTop;
    let scroll_position = height + scroll_top;
    let proximity = 100;

    if ((scroll_height - scroll_position) / scroll_height <= proximity) {
        await set_tweet_col();
    }
    run_scroll_update = false;
});


document.getElementById("image_view_close").addEventListener('click', function () {
    document.getElementById("images_view").style.display = "none";
});

