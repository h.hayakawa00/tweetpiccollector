async function set_authorlist(name_filter = "") {
    let selected_user = document.getElementsByClassName("user selected")[0];
    let authors;

    while(run_eel){
        await sleep(50);
    }
    run_eel=true;
    if (selected_user.id == "user_all") {
        authors = await eel.get_authors(name_filter)();
    } else {
        authors = await eel.get_authors(name_filter, selected_user.id.match(/user(.+)/)[1])();
    }
    run_eel=false;

    let ele_auhor_list = document.getElementById("author_list");
    ele_auhor_list.innerHTML = `<div class="author selected" id="author_all" onclick="onclick_author(this)"><div class="author_name">All Tweets</div></div>`;
    for (let author of authors) {
        let text = `
            <div class="author" id="author`+ author["id"] + `" onclick="onclick_author(this)">
                <div class="author_image_field">
                    <div class="author_image">
                        <img id="prof_img`+ author["id"] + `" src="` + author["image"] + `" alt="">
                    </div>
                </div>
                <div class="author_name">`+ author["name"] + `</div>
                <div class="author_screenname">@`+ author["screen_name"] + `</div>
            </div>`;
        ele_auhor_list.insertAdjacentHTML("beforeend", text);
    }

}

document.getElementById("author_search_input").addEventListener('input', function () {
    let name_filter = document.getElementById("author_search_input").value;
    set_authorlist(name_filter);
});

async function onclick_author(obj) {
    let selected_authors = document.getElementsByClassName("author selected");
    for (let selected of selected_authors) {
        selected.classList.remove("selected");
    }
    obj.classList.add("selected");
    reset_tweet_col();
}

