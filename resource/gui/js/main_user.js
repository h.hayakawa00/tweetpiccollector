async function get_users() {
    while (run_eel) {
        await sleep(50);
    }
    run_eel = true;
    let users = await eel.get_users()();
    run_eel = false;
    return users;
}
async function set_userlist() {
    let users = await get_users();
    let ele_user_list = document.getElementById("user_list");
    ele_user_list.innerHTML = `<div class="user selected" id="user_all" onclick="onclick_user(this)"><div class="user_image">All</div></div>`
    for (let user of users) {
        let text = `
            <div class="user" id="user`+ user["id"] + `" onclick="onclick_user(this)">
                <div class="user_image">
                    <img src="`+ user["image"] + `" alt="` + user["name"] + `" id="prof_img` + user["id"] + `">
                </div>
            </div>`;
        ele_user_list.insertAdjacentHTML("beforeend", text);
    }
    ele_user_list.insertAdjacentHTML("beforeend", `
        <div class="user" id="user_add">
            <div class="user_image">+</div>
        </div>
        <div class="user" id="setting">
            <div class="user_image">config</div>
        </div>`);
}

async function onclick_user(obj) {
    let selected_users = document.getElementsByClassName("user selected");
    for (let selected of selected_users) {
        selected.classList.remove("selected");
    }
    obj.classList.add("selected");

    let name_filter = document.getElementById("author_search_input").value;
    set_authorlist(name_filter);
}

function onclick_user_add(obj) {

}

