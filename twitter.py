import json
from logging import log
import time
import datetime

import requests
from requests_oauthlib import OAuth1Session

class TwitterApi:
    def __init__(
        self,
        CONSUMER_KEY,
        CONSUMER_SECRET,
        ACCESS_TOKEN,
        ACCESS_TOKEN_SECRET,
        log=False,
    ):
        self.CONSUMER_KEY = CONSUMER_KEY
        self.CONSUMER_SECRET = CONSUMER_SECRET
        self.ACCESS_TOKEN = ACCESS_TOKEN
        self.ACCESS_TOKEN_SECRET = ACCESS_TOKEN_SECRET
        self.log = log

        self.twitter = OAuth1Session(
            CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET
        )

        self.get_fav_list_max_count = 200

        self.rate_limit_status = dict()
        self.rate_limit_status["reset"] = -float("inf")
        self.rate_limit_status["remaining"] = float("inf")
        rate_limit = self.get_rate_limit_status()
        self.rate_limit_favorites_list = rate_limit["resources"]["favorites"][
            "/favorites/list"
        ]
        self.rate_limit_users_show = rate_limit["resources"]["users"]["/users/show/:id"]
        self.rate_limit_status = rate_limit["resources"]["application"][
            "/application/rate_limit_status"
        ]

    def get_fav_list(self, screen_name, count=200, max_id=None):
        if (
            self.rate_limit_favorites_list["remaining"] <= 0
            and self.rate_limit_favorites_list["reset"] >= time.time()
        ):
            until_reset = self.rate_limit_favorites_list["reset"] - time.time() + 10
            if self.log:
                self.__print_log(
                    "API access rate limit! Until restart {}seconds.".format(
                        until_reset
                    )
                )
            time.sleep(until_reset)

        url = "https://api.twitter.com/1.1/favorites/list.json"
        params = {
            "screen_name": screen_name,
            "count": count,
            "since_id":1,
            "include_entities": "true",
        }
        if max_id != None:
            params["max_id"] = max_id

        res = self.twitter.get(url, params=params)
        if res.status_code != 200:
            raise requests.exceptions.HTTPError()

        self.rate_limit_favorites_list["reset"] = int(res.headers["x-rate-limit-reset"])
        self.rate_limit_favorites_list["remaining"] = int(
            res.headers["x-rate-limit-remaining"]
        )

        r = json.loads(res.text)

        if self.log:
            self.__print_log("rate limit favorites\n"+str(self.rate_limit_favorites_list))
        return r

    def get_user(self, user_id=None, screen_name=None):
        if user_id == None and screen_name == None:
            raise ValueError(
                "get_user() missing 1 required positional argument: 'user_id' or 'screen_name'"
            )

        if (
            self.rate_limit_users_show["remaining"] <= 0
            and self.rate_limit_users_show["reset"] >= time.time()
        ):
            until_reset = self.rate_limit_users_show["reset"] - time.time() + 10
            if self.log:
                self.__print_log(
                    "API access rate limit! Until restart {}seconds.".format(
                        until_reset
                    )
                )
            time.sleep(until_reset)

        url = "https://api.twitter.com/1.1/users/show.json"
        params = {
            "include_entities": "true",
        }
        if user_id:
            params["user_id"] = user_id
        else:
            params["screen_name"] = screen_name

        res = self.twitter.get(url, params=params)
        if res.status_code != 200:
            raise requests.exceptions.HTTPError()

        self.rate_limit_users_show["reset"] = int(res.headers["x-rate-limit-reset"])
        self.rate_limit_users_show["remaining"] = int(
            res.headers["x-rate-limit-remaining"]
        )

        r = json.loads(res.text)

        if self.log:
            self.__print_log("rate limit users show\n"+str(self.rate_limit_users_show))
        return r

    def get_rate_limit_status(self, resources=None):
        if (
            self.rate_limit_status["remaining"] <= 0
            and self.rate_limit_status["reset"] >= time.time()
        ):
            until_reset = self.rate_limit_status["reset"] - time.time() + 10
            if self.log:
                self.__print_log(
                    "API access rate limit! Until restart {}seconds.".format(
                        until_reset
                    )
                )
            time.sleep(until_reset)

        url = "https://api.twitter.com/1.1/application/rate_limit_status.json"
        params = {}
        if resources:
            params["resources"] = ",".join(resources)

        res = self.twitter.get(url, params=params)
        if res.status_code != 200:
            raise requests.exceptions.HTTPError()

        self.rate_limit_status["reset"] = int(res.headers["x-rate-limit-reset"])
        self.rate_limit_status["remaining"] = int(res.headers["x-rate-limit-remaining"])

        r = json.loads(res.text)
        return r

    @classmethod
    def twitter_time_to_UNIXtime(self, twitter_time):
        result = datetime.datetime.strptime(
            twitter_time, "%a %b %d %H:%M:%S %z %Y"
        ).timestamp()
        return int(result)

    def __print_log(self,text):
        print("TwitterApi Class: " + text)

