from setting import *
from twitter import *
from store import *
from img_utils import *


class FavImageDownloader:
    def __init__(self, log=False):
        self.log = log
        self.APIKEY = config["api_key"]["twitter"]
        self.twitterapi = TwitterApi(
            self.APIKEY["CONSUMER_KEY"],
            self.APIKEY["CONSUMER_SECRET"],
            self.APIKEY["ACCESS_TOKEN"],
            self.APIKEY["ACCESS_TOKEN_SECRET"],
            log=log,
        )

    def fav_img_dl(self, screen_name, count_from_newer=200, quick_mode=False):
        user = get_user(screen_name)
        if user == None:
            user_inf = self.twitterapi.get_user(screen_name=screen_name)
            user_profile_image_url = user_inf["profile_image_url"]
            if "profile_image_url_https" in user_inf:
                user_profile_image_url = user_inf["profile_image_url_https"]
            user_profile_image_dir = os.path.join(
                EXEC_PATH, config["img_root_dir"], config["profile_image"]
            )
            user_profile_image_name = self.__save_tweet_image(
                user_profile_image_url, user_profile_image_dir
            )
            user_profile_image_path = os.path.join(
                config["img_root_dir"], config["profile_image"], user_profile_image_name
            )
            user = set_user(
                user_inf["name"],
                user_inf["screen_name"],
                user_profile_image_url,
                user_profile_image_path,
            )

        max_count = self.twitterapi.get_fav_list_max_count

        if count_from_newer <= max_count:
            favlist = self.twitterapi.get_fav_list(screen_name, count=count_from_newer)
            self.__store_image_from_favlist(favlist, user, quick_mode)
        else:
            count_lst = [max_count] * (count_from_newer // max_count)
            if count_from_newer % max_count != 0:
                count_lst.append(count_from_newer % max_count)
            max_id = None
            for count in count_lst:
                favlist = self.twitterapi.get_fav_list(
                    screen_name, count=count, max_id=max_id
                )
                self.__store_image_from_favlist(favlist, user, quick_mode)
                if favlist[0]["id"] == max_id and len(favlist) == 1 and count != 1:
                    if self.log:
                        self.__print_log("fav_img_dl_Complited")
                    break
                max_id = favlist[-1]["id"]

    def __store_image_from_favlist(self, favlist, user, quick_mode=False):
        for fav in favlist:
            if "entities" not in fav or "media" not in fav["entities"]:
                continue
            if fav["entities"]["media"][0]["type"] != "photo":
                continue
            if quick_mode:
                if get_tweet(int(fav["id"])) != None:
                    return

            if fav["user"]["screen_name"] == "uiCHOCOMO":
                print()

            author_inf = fav["user"]
            author = get_author(author_inf["id"])
            if author == None:
                author_profile_image_url = author_inf["profile_image_url"]
                if "profile_image_url_https" in author_inf:
                    author_profile_image_url = author_inf["profile_image_url_https"]
                author_profile_image_dir = os.path.join(
                    EXEC_PATH, config["img_root_dir"], config["profile_image"]
                )

                author_profile_image_name = self.__save_tweet_image(
                    author_profile_image_url, author_profile_image_dir
                )
                author_profile_image_path = os.path.join(
                    config["img_root_dir"],
                    config["profile_image"],
                    author_profile_image_name,
                )
                author = store_author(
                    int(author_inf["id"]),
                    author_inf["screen_name"],
                    author_inf["name"],
                    author_profile_image_url,
                    author_profile_image_path,
                )

            tweet_id = int(fav["id"])
            tweet_text = fav["text"]
            tweet_created_at = self.twitterapi.twitter_time_to_UNIXtime(
                fav["created_at"]
            )

            tweet = store_tweet(
                tweet_id, tweet_text, tweet_created_at, author.id, user.id
            )

            hashtags = self.__hashtag_analyzer(tweet_text)
            for hashtag in hashtags:
                tag = store_tag(tweet_id, hashtag)
                store_tweettag(tweet_id, tag.id)

            medias = fav["entities"]["media"]
            if "extended_entities" in fav:
                medias = fav["extended_entities"]["media"]

            for media in medias:
                if media["type"] not in ["photo", "animated_gif"]:
                    continue
                image_id = int(media["id"])
                if get_image(image_id) == None:
                    image_url = media["media_url"]
                    if "media_url_https" in media:
                        image_url = media["media_url_https"]
                    image_dir = os.path.join(
                        EXEC_PATH, config["img_root_dir"], tweet.author.screen_name
                    )
                    image_name = self.__save_tweet_image(
                        image_url, image_dir, orig=True
                    )
                    image_path = os.path.join(
                        config["img_root_dir"], tweet.author.screen_name, image_name
                    )
                    store_image(image_id, image_url, image_path, tweet.id)

            if self.log:
                self.__print_log("text:" + fav["text"])

    def __save_tweet_image(self, url, save_dir, orig=False):
        if orig:
            img = download_image(url + "?name=orig")
        else:
            img = download_image(url)
        ext = os.path.splitext(url)[1]
        img_name = re.match(r".+/([^/]+)" + ext + r"$", url).groups()[0]
        img_path = os.path.join(EXEC_PATH, save_dir, img_name + ext)
        save_image(img_path, img)
        return img_name + ext

    def __hashtag_analyzer(self, text):
        if text == "":
            return []
        splited_text = re.split(r"\s", text)
        splited_text = list(filter(lambda x: x != "", splited_text))
        hashtag = list()
        for s in splited_text:
            if s[0] == "#":
                hashtag.append(s)
        return hashtag

    def __print_log(self, text):
        print("FavImageDownloader Class: " + text)


if __name__ == "__main__":
    favimagedownloader = FavImageDownloader(log=True)
    favimagedownloader.fav_img_dl("on_the_ginger", 100,True)