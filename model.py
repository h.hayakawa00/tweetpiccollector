from os import name
import sys
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, DateTime
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import Text
from setting import Base
from setting import ENGINE


class User(Base):
    __tablename__ = "user"
    id = Column("id", Integer, primary_key=True, autoincrement=True, index=True)
    name = Column("name", String(200), nullable=False)
    screen_name = Column("screen_name", String(200), unique=True)
    profile_image_id = Column(
        String(100),
        ForeignKey("profile_image.id", onupdate="CASCADE", ondelete="CASCADE"),
        index=True,
    )
    tweet = relationship("Tweet", backref="user", uselist=True)


class Author(Base):
    __tablename__ = "author"
    id = Column("id", Integer, primary_key=True, index=True)
    screen_name = Column("screen_name", String(200), nullable=False)
    name = Column("name", String(200), nullable=False)
    profile_image_id = Column(
        String(100),
        ForeignKey("profile_image.id", onupdate="CASCADE", ondelete="CASCADE"),
        index=True,
    )
    tweet = relationship("Tweet", backref="author", uselist=True)


class ProfileImage(Base):
    __tablename__ = "profile_image"
    id = Column("id", Integer, primary_key=True, autoincrement=True)
    path = Column("path", String(200), nullable=False, unique=True)
    url = Column("url", String(100), nullable=False, unique=True)
    author = relationship("Author", backref="profile_image", uselist=True)
    user = relationship("User", backref="profile_image", uselist=True)


class Image(Base):
    __tablename__ = "image"
    id = Column("id", Integer, primary_key=True, index=True)
    path = Column("path", String(200), nullable=False, unique=True)
    url = Column("url", String(100), nullable=False, unique=True)
    tweet_id = Column(
        Integer,
        ForeignKey("tweet.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )


class Tweet(Base):
    __tablename__ = "tweet"
    id = Column("id", Integer, primary_key=True, index=True)
    text = Column("text", Text)
    created_at = Column("created_at", Integer, index=True)
    author_id = Column(
        Integer,
        ForeignKey("author.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )
    user_id = Column(
        Integer,
        ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )
    image = relationship("Image", backref="tweet", uselist=True)
    tweettag = relationship("TweetTag", backref="tweet", uselist=True)


class Tag(Base):
    __tablename__ = "tag"
    id = Column("id", Integer, primary_key=True, autoincrement=True, index=True)
    text = Column("text", String(100), nullable=False, unique=True)
    tweettag = relationship("TweetTag", backref="tag", uselist=True)


class TweetTag(Base):
    __tablename__ = "tweettag"
    tweet_id = Column(
        Integer,
        ForeignKey("tweet.id", onupdate="CASCADE", ondelete="CASCADE"),
        primary_key=True,
        index=True,
    )
    tag_id = Column(
        Integer,
        ForeignKey("tag.id", onupdate="CASCADE", ondelete="CASCADE"),
        primary_key=True,
        index=True,
    )

def main(args):
    """
    メイン関数
    """
    Base.metadata.create_all(bind=ENGINE)


main(sys.argv)