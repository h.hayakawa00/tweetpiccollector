import os
import base64
import cv2
import eel

from pyinstaller_utils import *
from setting import *
from model import Author, Image, ProfileImage, Tag, Tweet, TweetTag, User


@eel.expose
def get_users():
    users = session.query(User).order_by(User.name).all()
    users_list = []
    for user in users:
        user_dict = {}
        user_dict["id"] = str(user.id)
        user_dict["name"] = user.name
        user_dict["screen_name"] = user.screen_name
        user_dict["image_path"] = user.profile_image.path
        user_dict["image"] = get_local_image(user_dict["image_path"])
        users_list.append(user_dict)
    return users_list


@eel.expose
def get_authors(name_filter="", user_id=""):
    if name_filter != "":
        if name_filter[0] == "@":
            authors = (
                session.query(Author)
                .filter(Author.screen_name.like(str(name_filter[1:]) + "%"))
                .order_by(Author.name)
                .all()
            )
        else:
            authors = (
                session.query(Author)
                .filter(Author.name.like("%" + str(name_filter) + "%"))
                .order_by(Author.name)
                .all()
            )
    else:
        authors = session.query(Author).order_by(Author.name).all()
    authors_list = []
    if user_id != "":
        user_id = int(user_id)
        for author in authors:
            if author.tweet[0].user_id != user_id:
                continue
            author_dict = {}
            author_dict["id"] = str(author.id)
            author_dict["name"] = author.name
            author_dict["screen_name"] = author.screen_name
            author_dict["image_path"] = author.profile_image.path
            author_dict["image"] = get_local_image(author_dict["image_path"])
            authors_list.append(author_dict)
    else:
        for author in authors:
            author_dict = {}
            author_dict["id"] = str(author.id)
            author_dict["name"] = author.name
            author_dict["screen_name"] = author.screen_name
            author_dict["image_path"] = author.profile_image.path
            author_dict["image"] = get_local_image(author_dict["image_path"])
            authors_list.append(author_dict)

    return authors_list


tweets_cache = {}


@eel.expose
def get_tweets(count=50, max_created_at=-1, user_id="", author_id="", tags_id=[]):
    tags_id = tags_id = tuple(map(int, tags_id))
    if (count, max_created_at, user_id, author_id, tags_id) in tweets_cache:
        return tweets_cache[(count, max_created_at, user_id, author_id, tags_id)]

    tweets_query = session.query(Tweet)
    if max_created_at != -1:
        tweets_query = tweets_query.filter(Tweet.created_at < int(max_created_at))
    if user_id != "":
        tweets_query = tweets_query.filter(Tweet.user_id == int(user_id))
    if author_id != "":
        tweets_query = tweets_query.filter(Tweet.author_id == int(author_id))
    tweets = tweets_query.order_by(desc(Tweet.created_at)).limit(count).all()
    tweets_list = []
    for tweet in tweets:
        if tags_id != ():
            flag = True
            for tweettag in tweet.tweettag:
                if tweettag.tag_id in tags_id:
                    flag = False
                    continue
            if flag:
                continue
        tweet_dict = {}
        tweet_dict["id"] = str(tweet.id)
        tweet_dict["text"] = tweet.text
        author_dict = {}
        author_dict["id"] = str(tweet.author.id)
        author_dict["name"] = tweet.author.name
        author_dict["screen_name"] = tweet.author.screen_name
        author_dict["image_path"] = tweet.author.profile_image.path
        author_dict["image"] = get_local_image(author_dict["image_path"])
        tweet_dict["author"] = author_dict
        tweet_dict["user_id"] = str(tweet.user_id)
        tweet_dict["created_at"] = tweet.created_at
        tweet_tags = []
        for tweet_tag in tweet.tweettag:
            tag_dict = {}
            tag_dict["id"] = str(tweet_tag.tag.id)
            tag_dict["text"] = tweet_tag.tag.text
            tweet_tags.append(tag_dict)
        tweet_dict["tags"] = tweet_tags
        images = []
        for image in tweet.image:
            image_dict = {}
            image_dict["id"] = str(image.id)
            image_dict["path"] = image.path
            image_dict["image"] = get_local_image(image.path)
            images.append(image_dict)
        tweet_dict["images"] = images
        tweets_list.append(tweet_dict)
    tweets_cache[(count, max_created_at, user_id, author_id, tags_id)] = tweets_list

    return tweets_list


@eel.expose
def get_tags(count=20, index=""):
    tags = session.query(Tag).filter(Tag.text.like(index + "%")).limit(count).all()
    tags_list = []
    for tag in tags:
        tag_dict = {}
        tag_dict["id"] = str(tag.id)
        tag_dict["text"] = tag.text
        tags_list.append(tag_dict)
    return tags_list


@eel.expose
def get_tweet(id):
    tweets = session.query(Tweet).filter(Tweet.id == int(id)).all()
    if len(tweets) != 1:
        return None
    tweet = tweets[0]

    tweet_dict = {}
    tweet_dict["id"] = str(tweet.id)
    tweet_dict["text"] = tweet.text
    author_dict = {}
    author_dict["id"] = str(tweet.author.id)
    author_dict["name"] = tweet.author.name
    author_dict["screen_name"] = tweet.author.screen_name
    author_dict["image_path"] = tweet.author.profile_image.path
    author_dict["image"] = get_local_image(author_dict["image_path"], quality=100)
    tweet_dict["author"] = author_dict
    tweet_dict["user_id"] = str(tweet.user_id)
    tweet_dict["created_at"] = tweet.created_at
    tweet_tags = []
    for tweet_tag in tweet.tweettag:
        tag_dict = {}
        tag_dict["id"] = str(tweet_tag.tag.id)
        tag_dict["text"] = tweet_tag.tag.text
        tweet_tags.append(tag_dict)
    tweet_dict["tags"] = tweet_tags
    images = []
    for image in tweet.image:
        image_dict = {}
        image_dict["id"] = str(image.id)
        image_dict["path"] = image.path
        image_dict["image"] = get_local_image(image.path, quality=100)
        images.append(image_dict)
    tweet_dict["images"] = images

    return tweet_dict


def get_local_image(path, quality=50):
    path = os.path.join(EXEC_PATH, path)
    if not os.path.exists(path):
        print("not exists path: " + path)
        return None
    img = cv2.imread(path)
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), quality]
    _, imencode_image = cv2.imencode(".jpg", img, encode_param)
    base64_image = base64.b64encode(imencode_image)
    return "data:image/jpg;base64," + base64_image.decode()