import sys
import os


def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.dirname(__file__)
    return os.path.join(base_path, relative_path)


EXEC_PATH = os.path.split(sys.argv[0])[0]

if __name__ == "__main__":
    try:
        print("MEIPASS: "+sys._MEIPASS)
    except Exception:
        print("__file__: "+os.path.dirname(__file__))
    print("EXEC_PATH: "+EXEC_PATH)