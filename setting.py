import os
import json

from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.ext.declarative import declarative_base

from pyinstaller_utils import *


# configのロード
__config_path = os.path.join(EXEC_PATH, "config.json")
config = dict()
config["img_root_dir"] = "images/"
config["profile_image"] = ".profile_image/"
config["cache"] = ".cache/"
config["api_key"] = {
    "twitter": {
        "CONSUMER_KEY": "",
        "CONSUMER_SECRET": "",
        "ACCESS_TOKEN": "",
        "ACCESS_TOKEN_SECRET": "",
    }
}
if os.path.exists(__config_path):
    with open(__config_path) as f:
        config.update(json.load(f))
with open(__config_path, mode="w") as f:
    json.dump(config, f)

os.makedirs(os.path.join(EXEC_PATH, config["img_root_dir"]), exist_ok=True)
os.makedirs(
    os.path.join(EXEC_PATH, config["img_root_dir"], config["profile_image"]),
    exist_ok=True,
)

__DATABASE_PATH = os.path.join(EXEC_PATH, config["img_root_dir"], "db.sqlite3")

# mysqlのDBの設定
DATABASE = "sqlite:///" + __DATABASE_PATH
print(DATABASE)
ENGINE = create_engine(DATABASE, echo=True)  # Trueだと実行のたびにSQLが出力される

# Sessionの作成
session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=ENGINE))

# modelで使用する
Base = declarative_base()
Base.query = session.query_property()

# configの保存
def save_config(config):
    with open(__config_path, mode="w") as f:
        json.dump(config, f)